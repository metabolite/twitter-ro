function ready(fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(() => {
    console.log('Twitter RO initializing...');

    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    var observer = new MutationObserver(function(mutations, observer) {
        const actionElements = [
            'like',
            'retweet',
            'reply',
            'tweetButton',
            'tweetButtonInline',
        ].reduce((acc, id) => {
            return [
                ...acc,
                ...document.querySelectorAll(`[data-testid=${id}]`),
            ]
        }, []);

        actionElements.forEach((likeButton) => {
            likeButton.addEventListener('click', (event) => {
                event.stopPropagation()
            }, true);
        });
    });

    observer.observe(document, {
        childList: true,
        subtree: true,
    });
});
